package fr.atatorus.katadb

import fr.atatorus.katadb.KataDbError.*
import fr.atatorus.katadb.tx.IsolationLevel
import fr.atatorus.katadb.tx.Transaction
import java.sql.SQLException

class KataDbException private constructor(val error: KataDbError, message: String, cause: Throwable?) :
    RuntimeException(message, cause) {

    private constructor(error: KataDbError, message: String) : this(error, message, null)

    companion object {

        fun moreThanOneRow(sqlQuery: String) = KataDbException(
            moreThanOneRow,
            """
                This query has returned more than one rows :
                $sqlQuery
            """.trimIndent()
        )

        fun noRows(sqlQuery: String) = KataDbException(
            noRows,
            """
                This query has returned no rows :
                $sqlQuery
            """.trimIndent()
        )

        fun insertRows(sqlQuery: String) = KataDbException(
            insertError,
            """
                This query has inserted no rows :
                $sqlQuery
            """.trimIndent()
        )

        fun isolationLevelNotSupported(isolation: IsolationLevel) =
            KataDbException(isolationLevelNotSupported, "Isolation level $isolation is not supported")

        fun sqlException(exception: SQLException) =
            KataDbException(sqlException, "SQL exception threw executing query", exception)

        fun txIsRollbackOnly(tx: Transaction) =
            KataDbException(txRollbackOnly, "Transaction ${tx.name} is rollbackOnly")

        fun txIsAutocommit(tx: Transaction) =
            KataDbException(txAutocommit, "Transaction ${tx.name} is autocommit")

    }

}


enum class KataDbError {
    moreThanOneRow,
    noRows,
    insertError,
    isolationLevelNotSupported,
    sqlException,
    txRollbackOnly,
    txAutocommit
}