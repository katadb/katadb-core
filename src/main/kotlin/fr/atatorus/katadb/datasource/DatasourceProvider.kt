package fr.atatorus.katadb.datasource

import javax.sql.DataSource

interface DatasourceProvider {

    val datasource: DataSource

}
