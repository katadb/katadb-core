package fr.atatorus.katadb.tx

import fr.atatorus.katadb.KataDbException
import io.github.oshai.kotlinlogging.KotlinLogging
import java.sql.Connection
import java.sql.SQLException

private val logger = KotlinLogging.logger {}

class SimpleTransaction(
    override val name: String,
    override val autocommit: Boolean,
    override val isolation: IsolationLevel,
    val connection: Connection
) : Transaction {

    override var isRollbackOnly = false

    override fun <T> execute(query: Connection.() -> T): Result<T> = try {
        logger.info { "[$name] - Execute query" }
        Result.success(connection.query())
    } catch (e: SQLException) {
        logger.trace { "[$name] - Query error: $e" }
        isRollbackOnly = !connection.autoCommit
        Result.failure(KataDbException.sqlException(e))
    } catch (e: Exception) {
        isRollbackOnly = !connection.autoCommit
        logger.trace { "[$name] - Query error: $e" }
        Result.failure(e)
    }

    override fun commitAndClose() {
        commit()
        close()
    }

    override fun commit() {
        logger.trace { "[$name] - Commit transaction" }
        if (isRollbackOnly) {
            throw KataDbException.txIsRollbackOnly(this)
        }
        if (!connection.isClosed && !connection.autoCommit) {
            connection.commit()
            logger.trace { "[$name] - Transaction committed" }
        }
    }

    override fun rollback() {
        logger.trace { "[$name] - Rollback transaction" }
        if (connection.autoCommit) {
            throw KataDbException.txIsAutocommit(this)
        }
        connection.rollback()
        logger.trace { "[$name] - Transaction rollbacked" }
        close()
    }

    override fun isClosed() = connection.isClosed

    override fun close() {
        connection.close()
        logger.trace { "[$name] - Transaction closed" }
    }
}