package fr.atatorus.katadb.tx

import java.sql.Connection

/**
 * Used to set the isolation level of a [Connection].
 *
 * @property level according to [Connection] constants.
 * @constructor Create empty Isolation level
 */
enum class IsolationLevel(val level: Int) {

    /**
     * Not supported. Equivalent to [Connection#NO_TRANSACTION]
     */
    noTransaction(Connection.TRANSACTION_NONE), // 0
    /**
     * Equivalent to [Connection#TRANSACTION_READ_UNCOMMITTED]
     */
    readUncommitted(Connection.TRANSACTION_READ_UNCOMMITTED), // 1
    /**
     * Equivalent to [Connection#TRANSACTION_READ_COMMITTED]
     */
    readCommitted(Connection.TRANSACTION_READ_COMMITTED), // 2
    /**
     * Equivalent to [Connection#TRANSACTION_READ_COMMITTED]
     */
    repeatableRead(Connection.TRANSACTION_REPEATABLE_READ), // 4
    /**
     * Equivalent to [Connection#SERIALIZABLE]
     */
    serializable(Connection.TRANSACTION_SERIALIZABLE) // 8

}