package fr.atatorus.katadb.tx

import java.sql.Connection

interface Transaction {

    val name: String
    val autocommit: Boolean
    val isolation: IsolationLevel
    var isRollbackOnly: Boolean

    fun <T> execute(query: Connection.() -> T): Result<T>

    fun commitAndClose()

    fun rollback()

    fun isClosed(): Boolean

    fun close()

    fun commit()
}