package fr.atatorus.katadb.tx

import fr.atatorus.katadb.KataDbException
import fr.atatorus.katadb.tx.IsolationLevel.noTransaction
import fr.atatorus.katadb.tx.IsolationLevel.readCommitted
import javax.sql.DataSource

/**
 * To get a transaction
 */
interface TransactionProvider {

    /**
     * Returns a [Transaction]
     * @param name the name of transaction, used only for logs, default to "DefaultTxName"
     * @param autoCommit default to false
     * @param isolation default to [IsolationLevel.readCommitted]
     * @return a [Transaction]
     */
    fun getTransaction(name: String = "DefaultTxName", autoCommit: Boolean = false, isolation: IsolationLevel = readCommitted): Transaction

}

/**
 * A basic [TransactionProvider] that returns a new [Transaction] at each call to [TransactionProvider.getTransaction]
 */
class SimpleTransactionProvider(private val dataSource: DataSource) : TransactionProvider {

    override fun getTransaction(name: String, autoCommit: Boolean, isolation: IsolationLevel): Transaction {
        if (isolation == noTransaction) {
            throw KataDbException.isolationLevelNotSupported(isolation)
        }
        val connection = dataSource.connection.apply {
            this.transactionIsolation = isolation.level
            this.autoCommit = autoCommit
        }
        return SimpleTransaction(name, autoCommit, isolation, connection)
    }

}

/**
 * A provider returning the same [Transaction] for each call in the same Thread.
 */
class MultiThreadedTransactionProvider(private val datasource: DataSource) : TransactionProvider {

    private val transaction: ThreadLocal<Transaction> = ThreadLocal<Transaction>()

    override fun getTransaction(name: String, autoCommit: Boolean, isolation: IsolationLevel): Transaction {
        var tx = transaction.get()
        if (tx == null) {
            if (isolation == noTransaction) {
                throw KataDbException.isolationLevelNotSupported(isolation)
            }
            val cnx = datasource.connection.apply {
                this.transactionIsolation = isolation.level
                this.autoCommit = autoCommit
            }
            tx = SimpleTransaction(name, autoCommit, isolation, cnx)
            transaction.set(tx)
        } else if (tx.isClosed()) {
            val cnx = datasource.connection.apply {
                this.transactionIsolation = tx.isolation.level
                this.autoCommit = tx.autocommit
            }
            tx = SimpleTransaction(tx.name, tx.autocommit, tx.isolation, cnx)
            transaction.set(tx)
        }
        return tx
    }
}