package fr.atatorus.katadb.queries

import fr.atatorus.katadb.KataDbException
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet

/**
 * To find exactly one String.
 *
 * @param sql the sql query
 * @param parameters list of parameters
 * @throw KataDbException if the query returns no rows or more than one row
 * @return String?
 */
fun Connection.queryForString(sql: String, parameters: List<Any?> = listOf()) =
    initialize(sql, parameters).use { stmt ->
        stmt.getExactlyOneObject(sql, ResultSet::getString)
    }

/**
 * To find exactly one Int.
 *
 * @param sql the sql query
 * @param parameters list of parameters
 * @throw KataDbException if the query returns no rows or more than one row
 * @return Int?
 */
fun Connection.queryForInt(sql: String, parameters: List<Any?> = listOf()) = initialize(sql, parameters).use { stmt ->
    stmt.getExactlyOneObject(sql, ResultSet::getIntValue)
}

/**
 * To find exactly one long.
 *
 * @param sql the sql query
 * @param parameters list of parameters
 * @throw KataDbException if the query returns no rows or more than one row
 * @return Long?
 */
fun Connection.queryForLong(sql: String, parameters: List<Any?> = listOf()) = initialize(sql, parameters).use { stmt ->
    stmt.getExactlyOneObject(sql, ResultSet::getLongValue)
}

/**
 * To find exactly one boolean.
 *
 * @param sql the sql query
 * @param parameters list of parameters
 * @throw KataDbException if the query returns no rows or more than one row
 * @return Boolean?
 */
fun Connection.queryForBoolean(sql: String, parameters: List<Any?> = listOf()) = initialize(sql, parameters).use { stmt ->
    stmt.getExactlyOneObject(sql, ResultSet::getBooleanValue)
}

/**
 * To find exactly one local date.
 *
 * @param sql the sql query
 * @param parameters list of parameters
 * @throw KataDbException if the query returns no rows or more than one row
 * @return LocalDate?
 */
fun Connection.queryForDate(sql: String, parameters: List<Any?> = listOf()) = initialize(sql, parameters).use { stmt ->
    stmt.getExactlyOneObject(sql, ResultSet::getLocalDate)
}

/**
 * To find exactly one timestamp.
 *
 * @param sql the sql query
 * @param parameters list of parameters
 * @throw KataDbException if the query returns no rows or more than one row
 * @return LocalDateTime?
 */
fun Connection.queryForTimeStamp(sql: String, parameters: List<Any?> = listOf()) = initialize(sql, parameters).use { stmt ->
    stmt.getExactlyOneObject(sql, ResultSet::getLocalDateTime)
}

private fun <T> PreparedStatement.getExactlyOneObject(sql: String, map: ResultSet.(Int) -> T): T? {
    return executeQuery().use { rs ->
        if (rs.first()) {
            if (!rs.isLast) {
                throw KataDbException.moreThanOneRow(sql)
            }
            rs.map(1)
        } else {
            throw KataDbException.noRows(sql)
        }
    }
}

fun <T> Connection.queryForObjects(sql: String, parameters: List<Any?> = listOf(), mapper: (ResultSet) -> T) =
    initialize(sql, parameters).use { stmt ->
        val objects = arrayListOf<T>()
        stmt.executeQuery().use { rs ->
            while (rs.next()) {
                objects.add(mapper(rs))
            }
        }
        objects
    }

/**
 * To find exactly one object.
 *
 * @param sql the sql query
 * @param parameters list of parameters
 * @param mapper to map the result set to the expected object
 * @throw KataDbException if the query returns no rows or more than one row
 * @return an object of type T?
 */
fun <T> Connection.queryForObject(sql: String, parameters: List<Any?> = listOf(), mapper: (ResultSet) -> T) =
    initialize(sql, parameters).use { stmt ->
        stmt.executeQuery().use { rs ->
            if (rs.first()) {
                if (!rs.isLast) {
                    throw KataDbException.moreThanOneRow(sql)
                }
                mapper(rs)
            } else {
                throw KataDbException.noRows(sql)
            }
        }
    }

fun Connection.updateQuery(sql: String, parameters: List<Any?> = listOf()): Int =
    initialize(sql, parameters).use { stmt -> stmt.executeUpdate() }

fun Connection.insertQuery(sql: String, parameters: List<Any?> = listOf(), key: String = "id") =
    initialize(sql, parameters, key).use { stmt ->
        stmt.executeUpdate()
        stmt.generatedKeys.use { rs ->
            if (rs.next()) {
                rs.getLong(key)
            } else {
                throw RuntimeException("Insert error")
            }
        }
    }

fun Connection.batchInsertQuery(sql: String, parameters: List<List<Any?>> = listOf(), key: String = "id") =
    parameters.map { params ->
        initialize(sql, params, key).use { stmt ->
            stmt.executeUpdate()
            stmt.generatedKeys.use { rs ->
                if (rs.next()) {
                    rs.getLong("id")
                } else {
                    throw KataDbException.insertRows(sql)
                }
            }
        }
    }

/**
 * Create a [PreparedStatement] that returns autogenerated keys. Used only for insert queries.
 *
 * @param sql
 * @param parameters
 * @param key The column name of generated key
 * @return
 */
private fun Connection.initialize(sql: String, parameters: List<Any?>, key: String): PreparedStatement {
    return this.prepareStatement(sql, arrayOf(key)).also { stmt ->
        parameters.forEachIndexed { ix, it ->
            stmt.setObject(ix + 1, it)
        }
    }
}

/**
 * Create a [PreparedStatement] that could return many columns in a [ResultSet]. The result set will be scrollable,
 * not updatable, and closed at commit.
 *
 * @param sql
 * @param parameters
 * @return
 */
private fun Connection.initialize(sql: String, parameters: List<Any?> = listOf()): PreparedStatement {
    val rewriter = QueryRewriter(sql, parameters)
    return this.prepareStatement(
        rewriter.rewrittentQuery,
        ResultSet.TYPE_SCROLL_INSENSITIVE,
        ResultSet.CONCUR_READ_ONLY,
        ResultSet.CLOSE_CURSORS_AT_COMMIT
    ).also { stmt ->
        rewriter.flattenParameters.forEachIndexed { ix, it ->
            stmt.setObject(ix + 1, it)
        }
    }
}