package fr.atatorus.katadb.queries

import java.sql.ResultSet
import java.time.LocalDate
import java.time.LocalDateTime

fun ResultSet.getIntValue(position: Int): Int? {
    val i = this.getInt(position)
    return if (this.wasNull()) {
        null
    } else {
        i
    }
}

fun ResultSet.getLongValue(position: Int): Long? {
    val l = this.getLong(position)
    return if (this.wasNull()) {
        null
    } else {
        l
    }
}

fun ResultSet.getBooleanValue(position: Int): Boolean? {
    val b = this.getBoolean(position)
    return if (this.wasNull()) {
        null
    } else {
        b
    }
}

fun ResultSet.getLocalDate(position: Int): LocalDate? = getDate(position)?.toLocalDate()

fun ResultSet.getLocalDateTime(position: Int): LocalDateTime? = getTimestamp(position)?.toLocalDateTime()