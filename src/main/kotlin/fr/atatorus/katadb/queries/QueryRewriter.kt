package fr.atatorus.katadb.queries

/**
 * To modify an SQL query if the query has some parameters in a list for a clause IN.
 *
 * For example, the query:
 *
 * `SELECT * FROM table WHERE id = ? and name IN ? and age = ? AND city IN ?`
 *
 * with parameters
 *
 * `listOf(1, listOf("Marcel", "Henri", "Daniel"), null, listOf("Paris", null))`
 *
 * is rewritten to :
 *
 * `SELECT * FROM table WHERE id = ? and name IN (?,?,?) and age = ? AND city IN (?,?)`
 *
 * and parameters returned :
 *
 * `listOf(1, "Marcel", "Henri", "Daniel", null, "Paris", null)`
 *
 * @param query
 * @param parameters
 */
class QueryRewriter(query: String, parameters: List<Any?>) {

    /**
     * The query rewritten
     */
    val rewrittentQuery: String

    /**
     * Parameters flattened
     */
    val flattenParameters: List<Any?>

    init {
        var sql = query
        this.flattenParameters = parameters.flatMap { parameter ->
            if (parameter is Collection<*>) {
                sql = sql.replaceFirst(
                    "IN ?",
                    parameter.map { "?" }.joinToString(separator = ",", prefix = "IN (", postfix = ")")
                )
                parameter
            } else {
                listOf(parameter)
            }
        }
        this.rewrittentQuery = sql
    }
}