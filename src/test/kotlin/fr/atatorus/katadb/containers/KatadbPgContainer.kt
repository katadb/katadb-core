package fr.atatorus.katadb.containers

import org.junit.jupiter.api.extension.Extension
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testcontainers.containers.PostgreSQLContainer

class KatadbPgContainer(val image: String) : PostgreSQLContainer<KatadbPgContainer>(image), Extension {

    private val logger: Logger = LoggerFactory.getLogger("KatadbPgContainer - $image")

    override fun start() {
        super.start()
        logger.info("Container started")
    }

    override fun stop() {
        super.stop()
        logger.info("Container stopped")
    }

    companion object Images {
        const val POSTGRESQL_IMAGE = "postgres:17.0"
        const val MYSQL_IMAGE = "mysql:8.0"
        const val MARIADB_IMAGE = ""
    }
}
