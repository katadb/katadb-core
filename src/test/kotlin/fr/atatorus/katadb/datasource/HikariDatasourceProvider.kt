package fr.atatorus.katadb.datasource

import com.zaxxer.hikari.HikariDataSource

class HikariDatasourceProvider(
    jdbcUrl: String,
    user: String,
    password: String,
    configure: HikariDataSource.() -> Unit = {}
) :
    DatasourceProvider {

    override val datasource: HikariDataSource

    init {
        datasource = HikariDataSource().apply {
            this.jdbcUrl = jdbcUrl
            this.username = user
            this.password = password
            configure()
        }
    }

}