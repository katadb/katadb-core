package fr.atatorus.katadb

import fr.atatorus.katadb.containers.KatadbPgContainer
import fr.atatorus.katadb.datasource.HikariDatasourceProvider
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.RegisterExtension
import org.slf4j.Logger
import org.slf4j.LoggerFactory

abstract class PostgreSqlTest {

    companion object {

        val logger: Logger = LoggerFactory.getLogger(PostgreSqlTest::class.java)

        @JvmField
        @RegisterExtension
        val container = KatadbPgContainer(KatadbPgContainer.POSTGRESQL_IMAGE)

        @JvmStatic
        protected lateinit var datasourceProvider: HikariDatasourceProvider

        @JvmStatic
        @BeforeAll
        fun beforeAll() {
            logger.info(">>>>> Starting container...")
            container.start()
            logger.info(">>>>> Container started. Creating datasource...")
            datasourceProvider = HikariDatasourceProvider(container.jdbcUrl, container.username, container.password)
            logger.info(">>>>>  datasource created.")
        }

        @JvmStatic
        @AfterAll
        fun afterAll() {
            logger.info(">>>>>  Closing datasource...")
            datasourceProvider.datasource.close()
            logger.info(">>>>>  Datasource closed. Stopping container...")
            container.stop()
            logger.info(">>>>>  Container stopped")
        }
    }

    protected lateinit var initQueries: String
    protected lateinit var cleanQueries: String

    @BeforeEach
    fun initialize() {
        initQueries = readQueries("pg_init_database.sql")
        cleanQueries = readQueries("pg_clean_database.sql")
    }

    private fun readQueries(queriesFile: String): String {
        val reader =
            PostgreSqlTest::class.java.getResourceAsStream("/$queriesFile")?.bufferedReader() ?: throw RuntimeException(
                "Queries file $queriesFile not found"
            )
        return reader.readLines()
            .filter { line -> line.isNotEmpty() && !line.startsWith("--") }
            .joinToString("\n")
    }
}

