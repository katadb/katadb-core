package fr.atatorus.katadb.tx

import fr.atatorus.katadb.PostgreSqlTest
import fr.atatorus.katadb.queries.updateQuery
import fr.atatorus.katadb.utils.countCustomer
import fr.atatorus.katadb.utils.createHenriDusouris
import fr.atatorus.katadb.utils.createMarcelDurat
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.concurrent.CountDownLatch
import kotlin.concurrent.thread

class MultiThreadedTransactionProviderIT: PostgreSqlTest() {

    private lateinit var txProvider: MultiThreadedTransactionProvider

    @BeforeEach
    fun beforeEach() {
        txProvider = MultiThreadedTransactionProvider(datasourceProvider.datasource)
        val tx = txProvider.getTransaction(autoCommit = true)
        tx.apply {
            execute { updateQuery(cleanQueries) }
            execute { updateQuery(initQueries) }
        }
        tx.commitAndClose()
    }

    @Test
    fun `Les transactions ne doivent pas se mélanger entre deux threads`() {
        // Étant donné qu'on a deux transactions dans deux threads différents
        val cdl1a = CountDownLatch(1)
        val cdl1b = CountDownLatch(1)
        val cdl1c = CountDownLatch(1)
        val cdl1d = CountDownLatch(1)
        val cdl1e = CountDownLatch(1)

        val cdl2a = CountDownLatch(1)
        val cdl2b = CountDownLatch(1)
        val cdl2c = CountDownLatch(1)
        val cdl2d = CountDownLatch(1)

        thread {
            cdl1a.await()
            val tx = txProvider.getTransaction()
            cdl1b.await()
            val resultId = tx.createMarcelDurat()
            assertThat(resultId.isSuccess).isTrue()
            cdl1c.await()
            assertThat(tx.countCustomer().getOrNull()).isEqualTo(1)
            cdl1d.await()
            tx.commit()
            cdl1e.await()
            tx.close()
        }
        thread {
            cdl2a.await()
            val tx = txProvider.getTransaction()
            cdl2b.await()
            val resultId = tx.createHenriDusouris()
            assertThat(resultId.isSuccess).isTrue()
            cdl2c.await()
            assertThat(tx.countCustomer().getOrNull()).isEqualTo(1)
            cdl2d.await()
            assertThat(tx.countCustomer().getOrNull()).isEqualTo(2)
            tx.commitAndClose()
        }

        // Quand on les entrelace
        // Alors chaque transaction ne voit que ce qui la concerne
        // Get transactions
        cdl1a.countDown()
        cdl2a.countDown()
        // create customers
        cdl1b.countDown()
        cdl2b.countDown()
        // Count 1 customer
        cdl1c.countDown()
        cdl2c.countDown()
        // Thread 1 commit, thread 2 count 2 customers
        cdl1d.countDown()
        cdl2d.countDown()
        // FIN
        cdl1e.countDown()
    }


}