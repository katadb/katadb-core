package fr.atatorus.katadb.tx

import fr.atatorus.katadb.KataDbError
import fr.atatorus.katadb.KataDbException
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.sql.Connection
import java.sql.SQLException

class SimpleTransactionTest {

    private lateinit var connection: Connection
    private lateinit var tx: SimpleTransaction

    @BeforeEach
    fun beforeEach() {
        connection = mockk<Connection>()
        justRun { connection.commit() }
        justRun { connection.close() }
        justRun { connection.rollback() }
        tx = SimpleTransaction(
            name = "tx",
            autocommit = true,
            isolation = IsolationLevel.serializable,
            connection = connection
        )
    }

    @Test
    fun `Commiter une transaction commite la connection`() {
        // Étant donné qu'on a une transaction non auto commit
        every { connection.autoCommit } returns false
        every { connection.isClosed } returns false

        // Quand on la commite
        tx.commitAndClose()

        // Alors la connection est commitée et fermée
        verify(exactly = 1) { connection.commit() }
        verify(exactly = 1) { connection.close() }
    }

    @Test
    fun `Commiter une transaction autocommit ne fait que la fermer`() {
        // Étant donné qu'on a une transaction autoCommit
        every { connection.autoCommit } returns true
        every { connection.isClosed } returns false

        // Quand on la commit
        tx.commitAndClose()

        // Alors il ne se passe rien
        verify(exactly = 0) { connection.commit() }
        verify(exactly = 1) { connection.close() }
    }

    @Test
    fun `En cas d'exception la transaction est rollbackOnly`() {
        // Étant donné qu'on a une transaction
        every { connection.autoCommit } returns false

        // Quand ça péte
        val result = tx.execute { throw SQLException() }

        // Alors on est rollbackOnly
        assertThat(result.isFailure).isTrue()
        assertThat(tx.isRollbackOnly).isTrue()
    }

    @Test
    fun `Commiter une transaction rollbackOnly provoque une exception`() {
        // Étant donné qu'on a une transaction en rollback
        tx.isRollbackOnly = true

        // Quand on veut la commiter
        // Alors on a une exception
        assertThatThrownBy { tx.commitAndClose() }
            .isInstanceOf(KataDbException::class.java)
            .hasMessage("Transaction tx is rollbackOnly")
            .extracting("error").isEqualTo(KataDbError.txRollbackOnly)
    }

    @Test
    fun `Rollbacker une transaction autocommit provoque une exception`() {
        // Étant donné qu'on a une transaction autocommit
        every { connection.autoCommit } returns true

        // Quand on la rollbacke
        // Alors on a une exception
        assertThatThrownBy { tx.rollback() }
            .isInstanceOf(KataDbException::class.java)
            .hasMessage("Transaction tx is autocommit")
            .extracting("error").isEqualTo(KataDbError.txAutocommit)
    }

}