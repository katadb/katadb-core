package fr.atatorus.katadb.tx

import fr.atatorus.katadb.KataDbError
import fr.atatorus.katadb.KataDbException
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.sql.Connection
import javax.sql.DataSource

class SimpleTransactionProviderTest {

    private lateinit var txProvider: SimpleTransactionProvider
    private lateinit var datasource: DataSource
    private lateinit var connection: Connection

    @BeforeEach
    fun beforeEach() {
        datasource = mockk<DataSource>()
        connection = mockk<Connection>()
        every { datasource.connection } returns connection
        justRun { connection.setTransactionIsolation(any()) }
        justRun { connection.setAutoCommit(any()) }

        txProvider = SimpleTransactionProvider(datasource)
    }

    @Test
    fun `Il ne doit pas accepter le niveau NO_TRANSACTION`() {
        // Étant donné qu'on a un txProvider
        // Quand on lui demande une transaction non isolée
        // Alors on obtient une erreur
        assertThatThrownBy { txProvider.getTransaction(isolation = IsolationLevel.noTransaction) }
            .isInstanceOf(KataDbException::class.java)
            .hasMessage("Isolation level noTransaction is not supported")
            .extracting("error").isEqualTo(KataDbError.isolationLevelNotSupported)
    }

    @Test
    fun `Il doit fournir une transaction par défaut`() {
        // Étant donné qu'on a un txProvider
        // Quand on lui demande une transaction sans paramètres
        val tx = txProvider.getTransaction()

        // Alors il en renvoie une avec les valeurs par défaut
        assertThat(tx.name).isEqualTo("DefaultTxName")
        verify { connection.autoCommit = false }
        verify { connection.transactionIsolation = IsolationLevel.readCommitted.level }
    }

    @Test
    fun `Il doit fournir une transaction avec tous les bons paramètres`() {
        // Étant donné qu'on a un txProvider
        // Quand on lui demande une transaction avec un nom, autocommit et un niveau d'isolation
        val tx = txProvider.getTransaction(name = "tx", autoCommit = true, isolation = IsolationLevel.serializable)
        // Alors on récupère la transaction attendue
            assertThat(tx.name).isEqualTo("tx")
        verify { connection.autoCommit = true }
        verify { connection.transactionIsolation = IsolationLevel.serializable.level }
    }

}