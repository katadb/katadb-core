package fr.atatorus.katadb.tx

import fr.atatorus.katadb.KataDbError
import fr.atatorus.katadb.KataDbException
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.sql.Connection
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import javax.sql.DataSource
import kotlin.concurrent.thread

class MultiThreadedTransactionProviderTest {

    private lateinit var txProvider: MultiThreadedTransactionProvider
    private lateinit var datasource: DataSource
    private lateinit var connection: Connection

    @BeforeEach
    fun beforeEach() {
        datasource = mockk<DataSource>()
        connection = mockk<Connection>()
        every { datasource.connection } returns connection
        justRun { connection.setTransactionIsolation(any()) }
        justRun { connection.setAutoCommit(any()) }
        justRun { connection.close() }

        txProvider = MultiThreadedTransactionProvider(datasource)
    }

    @Test
    fun `Il ne doit pas accepter le niveau NoTransaction`() {
        // Étant donné qu'on veut une transaction sans isolation
        // Quand on la demande
        // Alors on a une erreur
        assertThatThrownBy { txProvider.getTransaction(isolation = IsolationLevel.noTransaction) }
            .isInstanceOf(KataDbException::class.java)
            .hasMessage("Isolation level noTransaction is not supported")
            .extracting("error").isEqualTo(KataDbError.isolationLevelNotSupported)
    }

    @Test
    fun `Quand on demande une transaction on demande une connexion à la datasource`() {
        // Étant donné qu'on a besoin d'une transaction
        // Quand on en demande une
        val tx1 = txProvider.getTransaction(name = "tx", autoCommit = true, isolation = IsolationLevel.serializable)

        // Alors on a une transaction correctement configurée
        assertThat(tx1.name).isEqualTo("tx")
        assertThat(tx1.autocommit).isTrue()
        assertThat(tx1.isolation).isEqualTo(IsolationLevel.serializable)

        // et on demande une nouvelle connection à la datasource
        verify(exactly = 1) { datasource.connection }
        verify { connection.autoCommit = true }
        verify { connection.transactionIsolation = IsolationLevel.serializable.level }
    }

    @Test
    fun `Redemander une transaction renvoie la même sans demander de nouvelle connexion`() {
        // Étant donné qu'on a déjà une transaction
        val tx1 = txProvider.getTransaction(name = "tx", autoCommit = true, isolation = IsolationLevel.readCommitted)
        // avec une connexion ouverte
        every { connection.isClosed } returns false
        clearMocks(datasource)

        // Quand on en demande une nouvelle
        val tx2 = txProvider.getTransaction()

        // Alors on obtient la même sans demander de nouvelle connexion
        assertThat(tx2).isSameAs(tx1)
        verify(exactly = 0) { datasource.connection }
    }

    @Test
    fun `Redemander une transaction quand la connexion est fermée en renvoie une nouvelle identique`() {
        // Étant donné qu'on a une transaction avec une connexion fermée
        every {connection.isClosed} returns false
        val tx1 = txProvider.getTransaction(name = "tx", autoCommit = true, isolation = IsolationLevel.repeatableRead)
        verify(exactly = 1) { datasource.connection }

        clearMocks(datasource)
        every {datasource.connection} returns connection
        every {connection.isClosed} returns true

        // Quand on demande une autre transaction depuis le même thread
        val tx2 = txProvider.getTransaction()

        // Alors on obtient une connexion identique
        assertThat(tx2).isNotSameAs(tx1)
        assertThat(tx2.name).isEqualTo(tx1.name)
        assertThat(tx2.autocommit).isEqualTo(tx1.autocommit)
        assertThat(tx2.isolation).isEqualTo(tx1.isolation)
        // et on demande une autre connexion à la datasource
        verify(exactly = 1) { datasource.connection }
        verify { connection.autoCommit = true }
        verify { connection.transactionIsolation = IsolationLevel.repeatableRead.level }
    }

    @Test
    fun `Quand on demande une transaction depuis un autre thread on en obtient une nouvelle`() {
        // Étant donné qu'on a demandé une transaction depuis un thread
        every {connection.isClosed} returns false
        txProvider.getTransaction(name = "tx", autoCommit = true, isolation = IsolationLevel.repeatableRead)
        verify(exactly = 1) { datasource.connection }
        clearMocks(datasource)
        every {datasource.connection} returns connection

        // Quand on en demande une depuis un autre thread
        lateinit var tx: Transaction
        val countdown = CountDownLatch(1)
        thread {
            tx = txProvider.getTransaction(name = "tx2", autoCommit = false, isolation = IsolationLevel.serializable)
            countdown.countDown()
        }
        countdown.await(100, TimeUnit.MILLISECONDS)

        // Alors on obtient
        assertThat(tx.name).isEqualTo("tx2")
        assertThat(tx.autocommit).isFalse()
        assertThat(tx.isolation).isEqualTo(IsolationLevel.serializable)
        // et on redemande une connection à la datasource
        verify(exactly = 1) { datasource.connection }
        verify { connection.autoCommit = false }
        verify { connection.transactionIsolation = IsolationLevel.serializable.level }
    }

}