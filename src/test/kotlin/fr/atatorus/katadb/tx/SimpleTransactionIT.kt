package fr.atatorus.katadb.tx

import fr.atatorus.katadb.KataDbError
import fr.atatorus.katadb.KataDbException
import fr.atatorus.katadb.PostgreSqlTest
import fr.atatorus.katadb.models.Customer
import fr.atatorus.katadb.queries.*
import fr.atatorus.katadb.utils.countCustomer
import fr.atatorus.katadb.utils.createHenriDusouris
import fr.atatorus.katadb.utils.createMarcelDurat
import fr.atatorus.katadb.utils.selectCustomerById
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class SimpleTransactionIT : PostgreSqlTest() {

    private lateinit var txProvider: SimpleTransactionProvider

    @BeforeEach
    fun beforeEach() {
        txProvider = SimpleTransactionProvider(datasourceProvider.datasource)
        val tx = txProvider.getTransaction(autoCommit = true)
        tx.apply {
            execute { updateQuery(cleanQueries) }
            execute { updateQuery(initQueries) }
        }
        tx.commitAndClose()
    }

    @Nested
    inner class AutoCommitTests {

        private lateinit var tx: Transaction

        @BeforeEach
        fun beforeEach() {
            tx = txProvider.getTransaction(autoCommit = true)
        }

        @AfterEach
        fun afterEach() {
            tx.close()
        }

        @Test
        fun `On peut créer et récupérer un enregistrement en mode autocommit`() {
            // Étant donné qu'on a une transaction
            // Quand on crée un customer
            val id = tx.createMarcelDurat()
            // alors ça marche
            assertThat(id.isSuccess).isTrue()

            // et quand on veut le récupérer
            val result = tx.selectCustomerById(id.getOrThrow())
            // alors on le récupère correctement
            assertThat(result.isSuccess).isTrue()
            result.onSuccess {
                assertThat(it!!.firstname).isEqualTo("Marcel")
                assertThat(it.lastname).isEqualTo("Durat")
                assertThat(it.email).isEqualTo("marcel@durat.com")
            }
        }

        @Test
        fun `On peut réutiliser une transaction autocommit même après une erreur`() {
            // Étant donné qu'on a une transaction et inséré un customer
            val id = tx.createMarcelDurat()
            id.onFailure { fail(it) }
            assertThat(id.isSuccess).isTrue()

            // quand on recommence avec un autre avec le même nom
            val result = tx.createMarcelDurat()
            // alors on a une erreur
            assertThat(result.isFailure).isTrue()
            assertThat(result.exceptionOrNull())
                .isInstanceOf(KataDbException::class.java)
                .extracting("error").isEqualTo(KataDbError.sqlException)

            // et si on réutilise la même transaction
            val customer = tx.selectCustomerById(id.getOrThrow())
            // alors ça marche
            customer.onFailure { fail(it) }
            assertThat(customer.isSuccess).isTrue()
            customer.onSuccess {
                assertThat(it!!.firstname).isEqualTo("Marcel")
                assertThat(it.lastname).isEqualTo("Durat")
                assertThat(it.email).isEqualTo("marcel@durat.com")
                assertThat(it.age).isEqualTo(42)
                assertThat(it.isActive).isTrue()
            }
        }

        @Test
        fun `Une transaction autocommit n'est jamais rollbackOnly`() {
            // Étant donné qu'on a une transaction et inséré un customer
            val id = tx.createMarcelDurat()
            assertThat(id.isSuccess).isTrue()

            // quand on recommence avec un autre avec le même nom
            val result = tx.createMarcelDurat()

            // alors on a une erreur
            assertThat(result.isFailure).isTrue()
            // mais la transaction n'est pas rollbackOnly
            assertThat(tx.isRollbackOnly).isFalse()
        }
    }

    @Nested
    inner class TransactionalTests {

        lateinit var tx1: Transaction
        lateinit var tx2: Transaction

        @BeforeEach
        fun beforeEach() {
            tx1 = txProvider.getTransaction(name = "tx1", isolation = IsolationLevel.readCommitted)
            tx2 = txProvider.getTransaction(name = "tx2", isolation = IsolationLevel.readCommitted)
        }


        @AfterEach
        fun afterEach() {
            if (tx1.isRollbackOnly) {
                tx1.rollback()
            } else {
                tx1.commitAndClose()
            }
            if (tx2.isRollbackOnly) {
                tx2.rollback()
            } else {
                tx2.commitAndClose()
            }
        }

        @Test
        fun `Une transaction isolée ne doit pas voir ce qui se passe dans une autre transaction tant que celle ci n'est pas committée`() {
            // Étant donné qu'on a déjà un customer en base
            val id = tx1.createMarcelDurat()
            assertThat(id.isSuccess).isTrue()

            // Quand la deuxième compte les utilisateurs
            var count = tx2.countCustomer()

            // Alors la seconde ne voit pas le résultat de la première,
            assertThat(count.isSuccess).isTrue()
            assertThat(count.getOrNull()).isEqualTo(0)

            // sauf s'il est commité
            tx1.commitAndClose()
            count = tx2.countCustomer()
            assertThat(count.isSuccess).isTrue()
            assertThat(count.getOrNull()).isEqualTo(1)
        }

        @Test
        fun `En cas d'erreur, une transaction ne peut qu'être rollbackée et elle n'a aucun effet`() {
            // Étant donné qu'on a inséré un customer
            val id = tx1.createMarcelDurat()
            assertThat(id.isSuccess).isTrue()

            // Quand on insère un autre avec le même email
            val id2 = tx1.createMarcelDurat()

            // Alors on a une erreur
            assertThat(id2.isFailure).isTrue()
            // on ne peut que rollbacker la transaction
            assertThat(tx1.isRollbackOnly).isTrue()
            // et il ne s'est rien passé
            val count = tx2.execute {
                queryForInt(
                    sql = "SELECT COUNT(*) FROM ${Customer.TABLE}"
                )
            }
            assertThat(count.isSuccess)
            assertThat(count.getOrThrow()).isEqualTo(0)
        }

        @Test
        fun `Une transaction commitée mais pas close peut être réutilisée`() {
            // Étant donné qu'on a une transaction
            val id = tx1.createMarcelDurat()
            assertThat(id.isSuccess).isTrue()

            // Quand on la commite
            tx1.commit()

            // Alors on peut la réutiliser
            tx1.createHenriDusouris()

            // et on a nos deux customers en base
            val count = tx1.countCustomer()
            count.onFailure { fail(it) }
            assertThat(count.getOrNull()).isEqualTo(2)
        }
    }

}