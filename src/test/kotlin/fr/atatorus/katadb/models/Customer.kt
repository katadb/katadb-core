package fr.atatorus.katadb.models

import java.sql.ResultSet
import java.time.LocalDate
import java.time.LocalDateTime

data class Customer(
    val id: Long?,
    val firstname: String?,
    val lastname: String?,
    val email: String,
    val age: Int?,
    val isActive: Boolean?,
    val dateOfBirth: LocalDate?,
    val lastConnection: LocalDateTime?
) {

    companion object {
        const val TABLE = "customer"

        val mapper = { rs: ResultSet ->
            val id = rs.getLong(1)
            val firstName = rs.getString(2)
            val lastName = rs.getString(3)
            val email = rs.getString(4)
            var age: Int? = rs.getInt(5)
            if (rs.wasNull()) {
                age = null
            }
            var isActive: Boolean? = rs.getBoolean(6)
            if (rs.wasNull()) {
                isActive = null
            }
            val dateOfBirth: LocalDate? = rs.getDate(7)?.toLocalDate()
            val lastConnection: LocalDateTime? = rs.getTimestamp(8)?.toLocalDateTime()

            Customer(id, firstName, lastName, email, age, isActive, dateOfBirth, lastConnection)
        }

    }
}