package fr.atatorus.katadb.models

data class Address(
    val id: Long?,
    val street: String?,
    val zip: String?,
    val city: String,
    val country: String?,
    val customerId: Long
) {

    companion object {
        const val TABLE = "address"
    }
}
