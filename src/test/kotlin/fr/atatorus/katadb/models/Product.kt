package fr.atatorus.katadb.models

data class Product(val id: Long?, val name: String, val description: String?, val price: Double) {

    companion object {
        const val TABLE = "product"
    }
}
