package fr.atatorus.katadb.models

import java.time.Instant

data class Order(val id: Long?, val date: Instant, val customerId: Long, val deleiveruAddressId: Long) {

    companion object {
        const val TABLE = "orders"
    }
}
