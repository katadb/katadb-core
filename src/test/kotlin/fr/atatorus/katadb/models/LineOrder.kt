package fr.atatorus.katadb.models

data class LineOrder(val id: Long?, val productId: Long, val quantity: Int, val rank: Int, val orderId: Long) {

    companion object {
        const val TABLE = "line_orders"
    }
}
