package fr.atatorus.katadb.queries

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class QueryRewritterTest {

    @Test
    fun notRewriteQueryIfNoListParametersTest() {
        val rewriter = QueryRewriter("SELECT * FROM table WHERE id = ?", listOf(1))

        assertThat(rewriter.flattenParameters.size).isEqualTo(1)
        assertThat(rewriter.flattenParameters[0]).isEqualTo(1)

        assertThat(rewriter.rewrittentQuery).isEqualTo("SELECT * FROM table WHERE id = ?")
    }

    @Test
    fun rewriteQueryTest() {
        val rewriter = QueryRewriter("SELECT * FROM table WHERE id IN ?", listOf(listOf(1, null, 2)))

        assertThat(rewriter.flattenParameters.size).isEqualTo(3)
        assertThat(rewriter.flattenParameters[0]).isEqualTo(1)
        assertThat(rewriter.flattenParameters[1]).isNull()
        assertThat(rewriter.flattenParameters[2]).isEqualTo(2)

        assertThat(rewriter.rewrittentQuery).isEqualTo("SELECT * FROM table WHERE id IN (?,?,?)")
    }

    @Test
    fun rewriteQueryWithManyParameterTest() {

        val rewriter = QueryRewriter("SELECT * FROM table WHERE id = ? and name IN ? and age = ?", listOf(1, listOf("Marcel", "Henri", null), null))

        assertThat(rewriter.flattenParameters.size).isEqualTo(5)
        assertThat(rewriter.flattenParameters[0]).isEqualTo(1)
        assertThat(rewriter.flattenParameters[1]).isEqualTo("Marcel")
        assertThat(rewriter.flattenParameters[2]).isEqualTo("Henri")
        assertThat(rewriter.flattenParameters[3]).isNull()
        assertThat(rewriter.flattenParameters[4]).isNull()

        assertThat(rewriter.rewrittentQuery).isEqualTo("SELECT * FROM table WHERE id = ? and name IN (?,?,?) and age = ?")
    }

    @Test
    fun rewriteQueryWithManyParameterListsTest() {
        val rewriter = QueryRewriter("SELECT * FROM table WHERE id = ? and name IN ? and age = ? AND city IN ?",
                                     listOf(1, listOf("Marcel", "Henri", null), null, listOf("Paris", "Bordeaux")))

        assertThat(rewriter.flattenParameters.size).isEqualTo(7)
        assertThat(rewriter.flattenParameters[0]).isEqualTo(1)
        assertThat(rewriter.flattenParameters[1]).isEqualTo("Marcel")
        assertThat(rewriter.flattenParameters[2]).isEqualTo("Henri")
        assertThat(rewriter.flattenParameters[3]).isNull()
        assertThat(rewriter.flattenParameters[4]).isNull()
        assertThat(rewriter.flattenParameters[5]).isEqualTo("Paris")
        assertThat(rewriter.flattenParameters[6]).isEqualTo("Bordeaux")

        assertThat(rewriter.rewrittentQuery).isEqualTo("SELECT * FROM table WHERE id = ? and name IN (?,?,?) and age = ? AND city IN (?,?)")
    }


}