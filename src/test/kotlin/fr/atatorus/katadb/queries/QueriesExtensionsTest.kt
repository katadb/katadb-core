package fr.atatorus.katadb.queries

import fr.atatorus.katadb.KataDbError
import fr.atatorus.katadb.KataDbException
import fr.atatorus.katadb.PostgreSqlTest
import fr.atatorus.katadb.models.Customer
import fr.atatorus.katadb.tx.SimpleTransactionProvider
import fr.atatorus.katadb.tx.Transaction
import fr.atatorus.katadb.tx.TransactionProvider
import fr.atatorus.katadb.utils.*
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class QueriesExtensionsTest : PostgreSqlTest() {

    private lateinit var txProvider: TransactionProvider
    private lateinit var tx: Transaction

    @BeforeEach
    fun beforeEach() {
        txProvider = SimpleTransactionProvider(datasourceProvider.datasource)
        tx = txProvider.getTransaction(autoCommit = true)
        tx.execute { updateQuery(cleanQueries) }
        tx.execute { updateQuery(initQueries) }
    }

    @AfterEach
    fun afterEach() {
        tx.close()
    }

    @Nested
    inner class InsertAndSelectQueryTest {
        @Test
        fun `On peut écrire et récupérer des enregistrements en base`() {
            // Quand on exécute un INSERT
            val id = tx.execute {
                insertQuery(
                    sql = """
                    INSERT INTO ${Customer.TABLE} (
                        firstname, lastname, email, age, is_active, date_of_birth, last_connection
                    ) VALUES (
                        ?, ?, ?, ?, ?, ?, ?
                    )
                """.trimIndent(),
                    parameters = listOf(marcel, durat, marcelEmail, marcelAge, marcelActive, marcelDateOfBirth, marcelLastConnection)
                )
            }
            // Alors c'est un succès
            assertThat(id.isSuccess).isTrue()
            // et on récupère l'id
            assertThat(id.getOrThrow()).isNotNull()

            // Et quand on exécute le SELECT
            val result = tx.execute {
                queryForObject(
                    sql = "SELECT id, firstname, lastname, email, age, is_active, date_of_birth, last_connection FROM ${Customer.TABLE} WHERE id = ?",
                    parameters = listOf(id.getOrNull()),
                    mapper = Customer.mapper
                )
            }
            // Alors c'est un succès
            assertThat(id.isSuccess).isTrue()
            // et on récupère l'objet voulu
            result.onSuccess {
                assertThat(it.id).isNotNull()
                assertThat(it.firstname).isEqualTo(marcel)
                assertThat(it.lastname).isEqualTo(durat)
                assertThat(it.email).isEqualTo(marcelEmail)
                assertThat(it.age).isEqualTo(marcelAge)
                assertThat(it.isActive).isTrue()
                assertThat(it.dateOfBirth).isEqualTo(marcelDateOfBirth)
                assertThat(it.lastConnection).isEqualTo(marcelLastConnection)
            }
        }

        @Test
        fun `On peut écrire plusieurs enregistrements en une seule instruction`() {
            // Quand on crée deux utilisateurs en une seule fois
            val ids = create2Customers()
            assertThat(ids.size).isEqualTo(2)
            val result = tx.execute {
                queryForObjects(
                    sql = "SELECT id, firstname, lastname, email, age, is_active, date_of_birth, last_connection FROM ${Customer.TABLE} WHERE id IN ?",
                    parameters = listOf(ids),
                    mapper = Customer.mapper
                )
            }
            // Alors c'est un succès
            assertThat(result.isSuccess).isTrue()
            // et on obtient les objets voulus
            result.onSuccess { customers ->
                customers.first { it.firstname == marcel }.apply {
                    assertThat(id).isNotNull()
                    assertThat(firstname).isEqualTo(marcel)
                    assertThat(lastname).isEqualTo(durat)
                    assertThat(email).isEqualTo(marcelEmail)
                    assertThat(age).isEqualTo(marcelAge)
                    assertThat(isActive).isTrue()
                    assertThat(dateOfBirth).isEqualTo(marcelDateOfBirth)
                    assertThat(lastConnection).isEqualTo(marcelLastConnection)
                }

                customers.first { it.firstname == henri }.apply {
                    assertThat(id).isNotNull()
                    assertThat(firstname).isEqualTo(henri)
                    assertThat(lastname).isEqualTo(dusouris)
                    assertThat(email).isEqualTo(henriEmail)
                    assertThat(age).isEqualTo(henriAge)
                    assertThat(isActive).isNull()
                    assertThat(dateOfBirth).isEqualTo(henriDateOfBirth)
                    assertThat(lastConnection).isEqualTo(henriLastConnection)
                }
            }
        }

    }

    @Nested
    inner class QueryForStringTest {

        @Test
        fun `On peut directement récupérer une String`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on cherche une String précise
            val result = tx.execute {
                queryForString(
                    sql = "SELECT email FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, durat)
                )
            }

            // Alors on la récupère
            assertThat(result.getOrThrow()).isEqualTo(marcelEmail)
        }

        @Test
        fun `On peut récupérer une String même si elle est null`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()
            // Dont un avec une String null
            tx.execute {
                updateQuery(
                    sql = "UPDATE ${Customer.TABLE} SET lastname = ? WHERE firstname = ?",
                    parameters = listOf(null, marcel)
                )
            }

            // Quand on récupère la String null
            val result = tx.execute {
                queryForString(
                    sql = "SELECT lastname FROM ${Customer.TABLE} WHERE firstname = ?",
                    parameters = listOf(marcel)
                )
            }

            // Alors ça fonctionne
            assertThat(result.isSuccess).isTrue()
            assertThat(result.getOrThrow()).isNull()
        }

        @Test
        fun `Récupérer une String pour un enregistrement qui n'existe pas provoque une exception`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on cherche un String pour un enregistrement inexistant
            val result = tx.execute {
                queryForString(
                    sql = "SELECT email FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, dusouris)
                )
            }

            // Alors on a une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.noRows)
            }
        }

        @Test
        fun `Récupérer plusieurs String provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on cherche une String, mais qu'il y en a plusieurs
            val result = tx.execute {
                queryForString(
                    sql = "SELECT email FROM ${Customer.TABLE}"
                )
            }

            // Alors on a une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.moreThanOneRow)
            }
        }
    }

    @Nested
    inner class QueryForIntTest {

        @Test
        fun `On peut récupérer directement un Int en base`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on veut récupérer
            val age = tx.execute {
                queryForInt(
                    sql = "SELECT age FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, durat)
                )
            }

            assertThat(age.getOrThrow()).isEqualTo(marcelAge)
        }

        @Test
        fun `On peut récupérer un Int null`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()
            // dont un avec un Int null
            tx.execute {
                updateQuery(
                    sql = "UPDATE ${Customer.TABLE} SET age = ? WHERE firstname = ?",
                    parameters = listOf(null, marcel)
                )
            }

            // Quand on le récupère
            val age = tx.execute {
                queryForInt(
                    sql = "SELECT age FROM ${Customer.TABLE} WHERE firstname = ?",
                    parameters = listOf(marcel)
                )
            }

            // Alors il est null
            assertThat(age.getOrThrow()).isNull()
        }

        @Test
        fun `Récupérer un Int qui n'existe pas provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on veut récupérer un Int pour un enregistrement qui n'existe pas
            val result = tx.execute {
                queryForInt(
                    sql = "SELECT age FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, dusouris)
                )
            }

            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.noRows)
            }
        }

        @Test
        fun `Récupérer plusieurs Int provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on essaye de récupérer plusieurs Int
            val result = tx.execute {
                queryForInt(
                    sql = "SELECT age FROM ${Customer.TABLE}"
                )
            }

            // Alors on a une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.moreThanOneRow)
            }
        }

    }

    @Nested
    inner class QueryForLongTest {

        @Test
        fun `On peut récupérer directement un Long en base`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on veut récupérer un Long
            val age = tx.execute {
                queryForLong(
                    sql = "SELECT age FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, durat)
                )
            }

            // Alors on obtient ce Long
            assertThat(age.getOrThrow()).isEqualTo(42L)
        }

        @Test
        fun `On peut récupérer un Long null`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()
            // dont un avec un Long null
            tx.execute {
                updateQuery(
                    sql = "UPDATE ${Customer.TABLE} SET age = ? WHERE firstname = ?",
                    parameters = listOf(null, marcel)
                )
            }

            // Quand on récupère ce Long
            val age = tx.execute {
                queryForLong(
                    sql = "SELECT age FROM ${Customer.TABLE} WHERE firstname = ?",
                    parameters = listOf(marcel)
                )
            }

            // Alors il est null
            assertThat(age.getOrThrow()).isNull()
        }

        @Test
        fun `Récupérer un Long qui n'existe pas provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on veut récupérer un Long pour un enregistrement inexistant
            val result = tx.execute {
                queryForLong(
                    sql = "SELECT age FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, dusouris)
                )
            }

            // Alors on obtient une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.noRows)
            }
        }

        @Test
        fun `Récupérer plusieurs Long provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on essaye de récupérer plusieurs Long
            val result = tx.execute {
                queryForLong(
                    sql = "SELECT age FROM ${Customer.TABLE}"
                )
            }

            // Alors on a une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.moreThanOneRow)
            }
        }
    }

    @Nested
    inner class QueryForBooleanTest {
        @Test
        fun `On peut récupérer directement un Boolean en base`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on veut récupérer un Boolean
            val isActif = tx.execute {
                queryForBoolean(
                    sql = "SELECT is_active FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, durat)
                )
            }

            // Alors on obtient ce Boolean
            assertThat(isActif.getOrThrow()).isTrue()
        }

        @Test
        fun `On peut récupérer un Boolean null`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on récupère un Boolean null
            val isActif = tx.execute {
                queryForBoolean(
                    sql = "SELECT is_active FROM ${Customer.TABLE} WHERE firstname = ?",
                    parameters = listOf(henri)
                )
            }

            // Alors il est null
            assertThat(isActif.getOrThrow()).isNull()
        }

        @Test
        fun `Récupérer un Boolean qui n'existe pas provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on essaye de récupérer un Boolean pour un enregistrement inexistant
            val result = tx.execute {
                queryForBoolean(
                    sql = "SELECT is_active FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, dusouris)
                )
            }

            // Alors on obtient une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.noRows)
            }
        }

        @Test
        fun `Récupérer plusieurs Boolean provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on essaye de récupérer plusieurs Boolean
            val result = tx.execute {
                queryForBoolean(
                    sql = "SELECT is_active FROM ${Customer.TABLE}"
                )
            }

            // Alors on a une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.moreThanOneRow)
            }
        }
    }

    @Nested
    inner class QueryForDateTest {
        @Test
        fun `On peut récupérer directement une LocalDate en base`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on veut récupérer un Boolean
            val dateOfBirth = tx.execute {
                queryForDate(
                    sql = "SELECT date_of_birth FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, durat)
                )
            }

            // Alors on obtient ce Boolean
            assertThat(dateOfBirth.getOrThrow()).isEqualTo(marcelDateOfBirth)
        }

        @Test
        fun `On peut récupérer une LocalDate null`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()
            tx.execute {
                updateQuery(
                    sql = "UPDATE ${Customer.TABLE} SET date_of_birth = ? WHERE firstname = ?",
                    parameters = listOf(null, marcel)
                )
            }
            // Quand on récupère un Boolean null
            val dateOfBirth = tx.execute {
                queryForBoolean(
                    sql = "SELECT date_of_birth FROM ${Customer.TABLE} WHERE firstname = ?",
                    parameters = listOf(marcel)
                )
            }

            // Alors il est null
            assertThat(dateOfBirth.getOrThrow()).isNull()
        }

        @Test
        fun `Récupérer une LoacalDate qui n'existe pas provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on essaye de récupérer un Boolean pour un enregistrement inexistant
            val result = tx.execute {
                queryForBoolean(
                    sql = "SELECT date_of_birth FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, dusouris)
                )
            }

            // Alors on obtient une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.noRows)
            }
        }

        @Test
        fun `Récupérer plusieurs LocalDate provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on essaye de récupérer plusieurs Boolean
            val result = tx.execute {
                queryForBoolean(
                    sql = "SELECT date_of_birth FROM ${Customer.TABLE}"
                )
            }

            // Alors on a une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.moreThanOneRow)
            }
        }
    }

    @Nested
    inner class QueryForTimestampTest {
        @Test
        fun `On peut récupérer directement un timestamp en base`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on veut récupérer un Boolean
            val lastConnection = tx.execute {
                queryForTimeStamp(
                    sql = "SELECT last_connection FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, durat)
                )
            }

            // Alors on obtient ce Boolean
            assertThat(lastConnection.getOrThrow()).isEqualTo(marcelLastConnection)
        }

        @Test
        fun `On peut récupérer un timestamp null`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()
            tx.execute {
                updateQuery(
                    sql = "UPDATE ${Customer.TABLE} SET last_connection = ? WHERE firstname = ?",
                    parameters = listOf(null, marcel)
                )
            }
            // Quand on récupère un timestamp null
            val timestamp = tx.execute {
                queryForTimeStamp(
                    sql = "SELECT last_connection FROM ${Customer.TABLE} WHERE firstname = ?",
                    parameters = listOf(marcel)
                )
            }

            // Alors il est null
            assertThat(timestamp.getOrThrow()).isNull()
        }

        @Test
        fun `Récupérer un timestamp qui n'existe pas provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on essaye de récupérer un Boolean pour un enregistrement inexistant
            val result = tx.execute {
                queryForTimeStamp(
                    sql = "SELECT last_connection FROM ${Customer.TABLE} WHERE firstname = ? AND lastname = ?",
                    parameters = listOf(marcel, dusouris)
                )
            }

            // Alors on obtient une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.noRows)
            }
        }

        @Test
        fun `Récupérer plusieurs timestamp provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on essaye de récupérer plusieurs Boolean
            val result = tx.execute {
                queryForTimeStamp(
                    sql = "SELECT last_connection FROM ${Customer.TABLE}"
                )
            }

            // Alors on a une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.moreThanOneRow)
            }
        }
    }

    @Nested
    inner class UpdateQueryTest {
        @Test
        fun `On peut mettre à jour un enregistrement`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on en met un à jour
            val rowsUpdated = tx.execute {
                updateQuery(
                    sql = "UPDATE ${Customer.TABLE} SET email = ? WHERE firstname = ?",
                    parameters = listOf(marcelEmail, marcel)
                )
            }

            // Alors on sait combien d'enregistrements ont été mis à jour
            assertThat(rowsUpdated.getOrThrow()).isEqualTo(1)
            // Et ils le sont vraiment
            val result = tx.execute {
                queryForObject(
                    sql = "SELECT id, firstname, lastname, email, age, is_active, date_of_birth, last_connection FROM ${Customer.TABLE} WHERE firstname = ?",
                    parameters = listOf(marcel),
                    mapper = Customer.mapper
                )
            }
            assertThat(result.isSuccess).isTrue()
            result.onSuccess { customer ->
                assertThat(customer.id)
                assertThat(customer.firstname).isEqualTo(marcel)
                assertThat(customer.lastname).isEqualTo(durat)
                assertThat(customer.email).isEqualTo(marcelEmail)
            }
        }

        @Test
        fun `On peut mettre une valeur à null`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on met une valeur à null
            val rowsUpdated = tx.execute {
                updateQuery(
                    sql = "UPDATE ${Customer.TABLE} SET age = ? WHERE firstname = ?",
                    parameters = listOf(null, marcel)
                )
            }

            // Alors on n'en a mis qu'une à null
            assertThat(rowsUpdated.getOrThrow()).isEqualTo(1)
            // et elle est vraiment à null
            val result = tx.execute {
                queryForObject(
                    sql = "SELECT id, firstname, lastname, email, age, is_active, date_of_birth, last_connection FROM ${Customer.TABLE} WHERE firstname = ?",
                    parameters = listOf(marcel),
                    mapper = Customer.mapper
                )
            }
            assertThat(result.isSuccess).isTrue()
            result.onSuccess { customer ->
                assertThat(customer.id).isNotNull()
                assertThat(customer.firstname).isEqualTo(marcel)
                assertThat(customer.lastname).isEqualTo(durat)
                assertThat(customer.email).isEqualTo(marcelEmail)
                assertThat(customer.age).isNull()
            }
        }
        @Test
        fun `On peut mettre à jour plusieurs valeurs`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on met à jour plusieurs enregistrements
            val rowsUpdated = tx.execute {
                updateQuery(
                    sql = "UPDATE ${Customer.TABLE} SET age = ? WHERE firstname IN ?",
                    parameters = listOf(50, listOf(marcel, henri))
                )
            }

            // Alors on les a tous mis à jour
            assertThat(rowsUpdated.getOrThrow()).isEqualTo(2)
            // et elles ont les bonnes valeurs
            val result = tx.execute {
                queryForObjects(
                    sql = "SELECT id, firstname, lastname, email, age, is_active, date_of_birth, last_connection FROM ${Customer.TABLE}",
                    mapper = Customer.mapper
                )
            }
            assertThat(result.isSuccess).isTrue()
            result.onSuccess { customers ->
                with(customers.first { it.firstname == marcel}) {
                    assertThat(id).isNotNull()
                    assertThat(firstname).isEqualTo(marcel)
                    assertThat(lastname).isEqualTo(durat)
                    assertThat(email).isEqualTo(marcelEmail)
                    assertThat(age).isEqualTo(50)
                    assertThat(isActive).isTrue()
                }
                with(customers.first { it.firstname == henri}) {
                    assertThat(id).isNotNull()
                    assertThat(firstname).isEqualTo(henri)
                    assertThat(lastname).isEqualTo(dusouris)
                    assertThat(email).isEqualTo(henriEmail)
                    assertThat(age).isEqualTo(50)
                    assertThat(isActive).isNull()
                }
            }
        }
    }

    @Nested
    inner class DeleteQueryTest {
        @Test
        fun `On peut supprimer un enregistrement`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on en supprimme un
            val rowsDeleted = tx.execute {
                updateQuery(
                    sql = "DELETE FROM ${Customer.TABLE} WHERE firstname = ?",
                    parameters = listOf(henri)
                )
            }

            // Alors on n'en a supprimé qu'un seul
            assertThat(rowsDeleted.getOrThrow()).isEqualTo(1)
            // et il n'existe plus en base
            val result = tx.execute {
                queryForObject(
                    sql = "SELECT id, firstname, lastname, email, age, is_active, date_of_birth, last_connection FROM ${Customer.TABLE}",
                    mapper = Customer.mapper
                )
            }
            assertThat(result.isSuccess).isTrue()
            result.onSuccess { customer ->
                assertThat(customer.id).isNotNull()
                assertThat(customer.firstname).isEqualTo(marcel)
                assertThat(customer.lastname).isEqualTo(durat)
                assertThat(customer.email).isEqualTo(marcelEmail)
                assertThat(customer.dateOfBirth).isEqualTo(marcelDateOfBirth)
                assertThat(customer.lastConnection).isEqualTo(marcelLastConnection)
            }
        }
    }

    @Nested
    inner class QueryForObjectTest {

        @Test
        fun `On peut récupérer un objet en base`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on veut en récupérer un
            val result = tx.execute {
                queryForObject(
                    sql = "SELECT id, firstname, lastname, email, age, date_of_birth, last_connection FROM ${Customer.TABLE} WHERE firstname = ?",
                    parameters = listOf(marcel),
                    mapper = Customer.mapper
                )
            }

            // Alors on le trouve
            assertThat(result.isSuccess)
            result.onSuccess { customer ->
                assertThat(customer.id).isNotNull()
                assertThat(customer.firstname).isEqualTo(marcel)
                assertThat(customer.lastname).isEqualTo(durat)
                assertThat(customer.email).isEqualTo(marcelEmail    )
                assertThat(customer.age).isEqualTo(42)
                assertThat(customer.dateOfBirth).isEqualTo(marcelDateOfBirth)
                assertThat(customer.lastConnection).isEqualTo(marcelLastConnection)
            }
        }

        @Test
        fun `Essayer de récupérer un objet qui n'existe pas renvoie provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on veut en récupérer un autre
            val customer = tx.execute {
                queryForObject(
                    sql = "SELECT id, firstname, lastname, email, age FROM ${Customer.TABLE} WHERE firstname = ?",
                    parameters = listOf("Pascal"),
                    mapper = Customer.mapper
                )
            }

            // Alors on obtient une erreur
            assertThat(customer.isFailure).isTrue()
            customer.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.noRows)
            }
        }

        @Test
        fun `Essayer de récupérer plusieurs objets provoque une erreur`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on essaye d'en récupérer plusieurs
            val result = tx.execute {
                queryForObject(
                    sql = "SELECT id, firstname, lastname, email, age FROM ${Customer.TABLE} WHERE firstname IN ?",
                    parameters = listOf(listOf(marcel, henri)),
                    mapper = Customer.mapper
                )
            }

            // Alors on obtient une erreur
            assertThat(result.isFailure).isTrue()
            result.onFailure {
                assertThat(it).isInstanceOf(KataDbException::class.java)
                    .extracting("error").isEqualTo(KataDbError.moreThanOneRow)
            }
        }
    }

    @Nested
    inner class QueryForObjects {

        @Test
        fun `On peut récupérer une liste d'objets`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on veut les récupérer
            val result = tx.execute {
                queryForObjects(
                    sql = "SELECT id, firstname, lastname, email, age, is_active, date_of_birth, last_connection FROM ${Customer.TABLE} WHERE firstname IN ?",
                    parameters = listOf(listOf(marcel, henri)),
                    mapper = Customer.mapper
                )
            }

            // Alors on les obtient
            assertThat(result.isSuccess).isTrue()
            val customers = result.getOrThrow()
            assertThat(customers.size).isEqualTo(2)
            with(customers.first { it.firstname == marcel }) {
                assertThat(id).isNotNull()
                assertThat(firstname).isEqualTo(marcel)
                assertThat(lastname).isEqualTo(durat)
                assertThat(email).isEqualTo(marcelEmail)
                assertThat(age).isEqualTo(42)
                assertThat(isActive).isTrue()
                assertThat(dateOfBirth).isEqualTo(marcelDateOfBirth)
                assertThat(lastConnection).isEqualTo(marcelLastConnection)
            }
            with(customers.first { it.firstname == henri }) {
                assertThat(id).isNotNull()
                assertThat(firstname).isEqualTo(henri)
                assertThat(lastname).isEqualTo(dusouris)
                assertThat(email).isEqualTo(henriEmail)
                assertThat(age).isEqualTo(43)
                assertThat(isActive).isNull()
                assertThat(dateOfBirth).isEqualTo(henriDateOfBirth)
                assertThat(lastConnection).isEqualTo(henriLastConnection)
            }
        }

        @Test
        fun `Récupérer des objets qui n'existent pas renvoie une liste vide`() {
            // Étant donné qu'on a plusieurs enregistrements en base
            create2Customers()

            // Quand on essaye d'en récupérer qui n'existe pas
            val customers = tx.execute {
                queryForObjects(
                    sql = "SELECT id, firstname, lastname, email, age, is_active, date_of_birth, last_connection FROM ${Customer.TABLE} WHERE age < ?",
                    parameters = listOf(20),
                    mapper = Customer.mapper
                )
            }

            // Alors on obtient une liste vide
            assertThat(customers.getOrThrow().isEmpty())
        }
    }

    private fun create2Customers(): List<Long> {
        val ids = tx.execute {
            batchInsertQuery(
                sql = """
                        INSERT INTO ${Customer.TABLE} (
                            firstname, lastname, email, age, is_active, date_of_birth, last_connection
                        ) VALUES (
                            ?, ?, ?, ?, ?, ?, ?
                        )
                    """.trimIndent(),
                parameters = listOf(
                    listOf(marcel, durat, marcelEmail, marcelAge, marcelActive, marcelDateOfBirth, marcelLastConnection),
                    listOf(henri, dusouris, henriEmail, henriAge, null, henriDateOfBirth, henriLastConnection),
                )
            )
        }
        return ids.getOrThrow()
    }
}