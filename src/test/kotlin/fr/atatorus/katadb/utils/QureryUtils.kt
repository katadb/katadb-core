package fr.atatorus.katadb.utils

import fr.atatorus.katadb.models.Customer
import fr.atatorus.katadb.queries.insertQuery
import fr.atatorus.katadb.queries.queryForInt
import fr.atatorus.katadb.queries.queryForObject
import fr.atatorus.katadb.tx.Transaction
import java.time.LocalDate
import java.time.LocalDateTime

const val marcel = "Marcel"
const val durat = "Durat"
const val marcelEmail = "marcel@durat.com"
const val marcelAge = 42
const val marcelActive = true
val marcelDateOfBirth = LocalDate.of(2001,2,3)
val marcelLastConnection = LocalDateTime.of(2001,2,3,4,5,6)

fun Transaction.createMarcelDurat(): Result<Long> = execute {
    insertQuery(
        sql = """
                    INSERT INTO ${Customer.TABLE} (
                        firstname, lastname, email, age, is_active, date_of_birth, last_connection
                    ) VALUES (
                        ?, ?, ?, ?, ?, ?, ?
                    )
                """.trimIndent(),
        parameters = listOf(marcel, durat, marcelEmail, marcelAge, marcelActive, marcelDateOfBirth, marcelLastConnection)
    )
}

const val henri = "Henri"
const val dusouris = "Dusouris"
const val henriEmail = "henri@dusouris.com"
const val henriAge = 43
val henriDateOfBirth = LocalDate.of(2002,3,4)
val henriLastConnection = LocalDateTime.of(2002,3,4,5,6, 7)

fun Transaction.createHenriDusouris(): Result<Long> = execute {
    insertQuery(
        sql = """
                    INSERT INTO ${Customer.TABLE} (
                        firstname, lastname, email, age, is_active, date_of_birth, last_connection
                    ) VALUES (
                        ?, ?, ?, ?, ?, ?, ?
                    )
                """.trimIndent(),
        parameters = listOf(henri, dusouris, henriEmail, henriAge, null, henriDateOfBirth, henriLastConnection)
    )
}

fun Transaction.selectCustomerById(id: Long): Result<Customer?> = execute {
    queryForObject(
        sql = "SELECT id, firstname, lastname, email, age, is_active, date_of_birth, last_connection FROM ${Customer.TABLE} WHERE id = ?",
        parameters = listOf(id),
        mapper = Customer.mapper
    )
}

fun Transaction.countCustomer() = execute {
    queryForInt("SELECT count(*) FROM ${Customer.TABLE}")
}