create table customer (
    id bigserial primary key,
    firstname varchar(50),
    lastname varchar(50),
    age Int,
    email varchar(50) not null,
    is_active boolean,
    date_of_birth date,
    last_connection timestamp
);
create unique index udx_customer_email on customer(email);

create table address (
    id bigserial primary key,
    street varchar(100),
    zip varchar(10),
    city varchar(50) not null,
    country varchar(50),
    customer_id bigint not null constraint fk_address_customer references customer(id)
);

create table product (
    id bigserial primary key,
    name varchar(50) not null,
    description varchar(100),
    price numeric not null
);
create unique index udx_product_name on product(name);

create table orders (
    id bigserial primary key,
    date timestamp not null,
    customer_id bigint not null constraint fk_order_customer references customer(id),
    delivery_address_id bigint not null constraint fk_order_delivery_adress references address(id)
);

create table line_orders (
    id bigserial primary key,
    product_id bigint not null constraint fk_line_order_product references product(id),
    quantity integer not null,
    rank integer not null,
    order_id bigint not null constraint fk_line_order_order references orders(id)
);
create unique index udx_line_orders_product on line_orders(product_id);
create unique index udx_line_orders_rank_order on line_orders(rank, order_id);