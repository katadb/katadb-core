val kotlin_version: String by project
val postgresql_version: String by project
val hikari_version: String by project
val kotlin_logging_version: String by project
val slf4j_version: String by project
val junit_version: String by project
val assertj_version: String by project
val testcontainer_version: String by project
val mockk_version: String by project

plugins {
    kotlin("jvm") version "2.0.20"
    id("idea")
}

idea {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

repositories {
    mavenLocal()
    maven {
        url = uri("https://repo.maven.apache.org/maven2/")
    }
}

dependencies {
    implementation("io.github.oshai:kotlin-logging-jvm:$kotlin_logging_version")
    testImplementation("org.slf4j:slf4j-simple:$slf4j_version")
    testImplementation("org.junit.jupiter:junit-jupiter:$junit_version")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
    testImplementation("com.zaxxer:HikariCP:$hikari_version")
    testImplementation("org.assertj:assertj-core:$assertj_version")
    testImplementation("org.postgresql:postgresql:$postgresql_version")
    testImplementation("org.testcontainers:junit-jupiter:$testcontainer_version")
    testImplementation("org.testcontainers:postgresql:$testcontainer_version")
    testImplementation("io.mockk:mockk-jvm:$mockk_version")
}

group = "fr.atatorus.katadb"
version = "0.1"
description = "fr.atatorus katadb-core"
java.sourceCompatibility = JavaVersion.VERSION_21

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(21)
}

tasks.withType<JavaCompile>() {
    options.encoding = "UTF-8"
}

tasks.withType<Javadoc>() {
    options.encoding = "UTF-8"
}